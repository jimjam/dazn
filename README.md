# DAZN Tech Test

An online demo of the app can be found [here](https://unalome.technology/).

## Installation

The app uses `yarn` as its package manager.

To install the app's dependencies please run:

```
yarn install
```

## Building

To build the app run:

```
yarn run build
```

This will build to the app `dist` directory.

You can then serve the app via a HTTP server e.g `http-server dist/`

## Developing

To run a development server via `webpack` run:

```
yarn run develop
```

## Testing

The app is tested using `jest` and `enzyme`.

To run the tests:

```
yarn run test
```
