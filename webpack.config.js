const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BabelPluginTransformClassProperties = require('babel-plugin-transform-class-properties');

// Plugin Configurations
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './public/index.html',
  filename: 'index.html',
  inject: 'body'
});

// Exported Configuration
module.exports = {
  mode: "none",
  devServer: {
    compress: true,
    port: 9001
  },
  devtool: 'cheap-module-source-map',
  entry: [
    'babel-polyfill',
    './src/main.js'
  ],
  output: {
    filename: '[name].bundle.js?[hash:8]',
    path: path.resolve('dist'),
    publicPath: '/'
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      enforce: 'pre',
      use: {
        loader: 'eslint-loader',
        options: {
          emitWarning: true
        }
      }
    },
    {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [
            'env',
            'react',
            'es2017'
          ],
          plugins: [
            BabelPluginTransformClassProperties,
          ],
          cacheDirectory: true
        }
      }
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader'
      ]
    },
    {
      test: /\.scss$/,
      exclude: /node_modules/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader',
        {
          loader: 'sass-resources-loader',
          options: {
            resources: path.resolve(__dirname, './src/main.scss')
          }
        }
      ]
    }]
  },
  plugins: [
    HtmlWebpackPluginConfig
  ],
  resolve: {
    extensions: [
      '.js',
      '.json',
      '.css',
      '.scss',
    ],
    modules: ['node_modules', path.resolve(__dirname, 'src')]
  }
};
