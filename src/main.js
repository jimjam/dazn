import React from 'react';
import ReactDOM from 'react-dom';
import 'reset-css';
import config from '../config';
import App from './components/app';
import movieService from './services/movieservice';

ReactDOM.render(<App movieService={movieService(config.moviedb)} />, document.getElementById('dazn'));
