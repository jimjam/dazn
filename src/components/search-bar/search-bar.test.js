import React from 'react';
import ReactDOM from 'react-dom';

import SearchBar from './index';

describe('<SearchBar />', () => {
  test('renders without crashing', () => {
    mount(<SearchBar onSearchChange={() => {}} />);
  });

  test('the onSearchChange callback is invoked', () => {
    const event = {
      target: {
        value: 'foo',
      },
    };
    const onSearchChangeMock = jest.fn();
    const component = shallow(<SearchBar onSearchChange={onSearchChangeMock} />);
    component.find('input').simulate('change', event);
    expect(onSearchChangeMock).toBeCalledWith(event);
  });
});
