import React from 'react';
import PropTypes from 'prop-types';
import './search-bar.scss';

/**
 * Describes the search bar component that is used for inputting the search term
 *
 * @param {Function} onSearchChange
 * @return {JSX}
 */
const SearchBar = ({ onSearchChange }) => (
  <div className="searchbar">
    <input
      type="text"
      onChange={onSearchChange}
      placeholder="Search for movies..."
      autoComplete="off"
      autoFocus
    />
  </div>
);

// Prop types definition
SearchBar.propTypes = {
  onSearchChange: PropTypes.func.isRequired,
};

export default SearchBar;
