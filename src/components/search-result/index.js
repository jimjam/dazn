import React from 'react';
import PropTypes from 'prop-types';
import './search-result.scss';
import SearchResultImage from '../search-result-image';

// Search result component
const SearchResult = ({ title, overview, poster }) => (
  <div className="result">
    <div className="result--header">
      {title}
    </div>
    <SearchResultImage poster={poster} />
    <div className="result--overview">
      {overview}
    </div>
  </div>
);

// Prop types definition
SearchResult.propTypes = {
  title: PropTypes.string.isRequired,
  overview: PropTypes.string.isRequired,
  poster: PropTypes.string,
};

// Default prop types
SearchResult.defaultProps = {
  poster: null,
};

export default SearchResult;
