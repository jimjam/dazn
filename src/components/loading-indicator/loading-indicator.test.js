import React from 'react';
import ReactDOM from 'react-dom';

import LoadingIndicator from './index';

describe('<LoadingIndicator />', () => {
  test('happy path', () => {
    const sut = shallow(<LoadingIndicator />);
    expect(sut).toHaveClassName('loading');
    expect(sut.children()).toHaveLength(5);
  });
});
