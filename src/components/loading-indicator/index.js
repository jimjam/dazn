import React from 'react';
import './loading-indicator.scss';

/**
 * Describes the loading indicator component
 *
 * @return {JSX}
 */
const LoadingIndicator = () => (
  <div className="loading">
    <div className="loading--1" />
    <div className="loading--2" />
    <div className="loading--3" />
    <div className="loading--4" />
    <div className="loading--5" />
  </div>
);

export default LoadingIndicator;
