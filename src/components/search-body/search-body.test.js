import React from 'react';
import ReactDOM from 'react-dom';

import SearchBody from './index';
import LoadingIndicator from '../loading-indicator';
import SearchResults from '../search-results';

describe('<SearchBody />', () => {
  test('renders without crashing', () => {
    mount(<SearchBody movies={[]} />);
  });

  test('renders a <LoadingIndicator /> when it is loading', () => {
    const sut = shallow(<SearchBody movies={[]} isLoading={true} />);
    expect(sut.find(LoadingIndicator)).toExist();
    expect(sut.find(SearchResults)).toHaveLength(0);
  });

  test('renders a <SearchResults /> when it is not loading', () => {
    const sut = shallow(<SearchBody movies={[]} isLoading={false} />);
    expect(sut.find(LoadingIndicator)).toHaveLength(0);
    expect(sut.find(SearchResults)).toExist();
  });

  test('renders a no results placeholder', () => {
    const sut = shallow(<SearchBody movies={[]} isLoading={false} searchTerm={'foo'} />);
    expect(sut.find(LoadingIndicator)).toHaveLength(0);
    expect(sut.find(SearchResults)).toHaveLength(0);
    expect(sut).toHaveClassName('no-results');
    expect(sut).toHaveText('No Movies Found');
  });

  test('renders a an error message', () => {
    const sut = shallow(<SearchBody movies={[]} isLoading={false} searchTerm={'foo'} hasError={true} />);
    expect(sut.find(LoadingIndicator)).toHaveLength(0);
    expect(sut.find(SearchResults)).toHaveLength(0);
    expect(sut).toHaveClassName('error');
    expect(sut).toHaveText('Something has gone wrong. Please try again');
  });
});
