import React from 'react';
import PropTypes from 'prop-types';
import LoadingIndicator from '../loading-indicator';
import SearchResults from '../search-results';
import './search-body.scss';

/**
 * Describes the search body where results or a loading indicator will be displayed
 *
 * @param {Boolean} isLoading
 * @param {Object[]} movies
 * @param {String} searchTerm
 * @param {Boolean} hasError
 * @returns {JSX}
 */
const SearchBody = ({
  isLoading,
  movies,
  searchTerm,
  hasError,
}) => {
  if (hasError) {
    return <div className="error">Something has gone wrong. Please try again</div>;
  }

  if (isLoading) {
    return <LoadingIndicator />;
  }

  if (!isLoading && movies.length < 1 && searchTerm.length > 0) {
    return <div className="no-results">No Movies Found</div>;
  }

  return <SearchResults movies={movies} />;
};

// Prop types definition
SearchBody.propTypes = {
  isLoading: PropTypes.bool,
  movies: PropTypes.arrayOf(PropTypes.object).isRequired,
  searchTerm: PropTypes.string,
  hasError: PropTypes.bool,
};

// Default prop types
SearchBody.defaultProps = {
  isLoading: false,
  searchTerm: '',
  hasError: false,
};

export default SearchBody;
