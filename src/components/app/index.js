import React from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import './app.scss';
import SearchBar from '../search-bar';
import SearchBody from '../search-body';

class App extends React.Component {
  static propTypes = {
    movieService: PropTypes.shape({
      search: PropTypes.func.isRequired,
    }).isRequired,
  };

  /**
   * Constructor
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      movies: [],
      searchTerm: '',
      hasError: false,
    };

    // Debounced search function
    this.debounceSearch = debounce(query => this.doSearch(query), 500);

    // Bindings
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  /**
   * Handles a change to the search bar and updates the app state
   *
   * @param {Proxy} event
   */
  onSearchChange(event) {
    const searchTerm = event.target.value;
    const searchTermHasLength = searchTerm.length > 0;

    this.setState({
      isLoading: searchTermHasLength,
      searchTerm,
      hasError: false,
    });

    if (searchTermHasLength) {
      event.persist();
      this.debounceSearch(searchTerm);
    } else {
      this.debounceSearch.cancel();

      this.setState({
        isLoading: false,
        searchTerm: '',
        hasError: false,
        movies: [],
      });
    }
  }

  /**
   * Perform a search via the movie service
   *
   * @param {String} query The query string
   * @param {Function} cb (optional) Callback invoked on completion
   */
  doSearch(query, cb) {
    const update = movies => this.setState({
      movies,
      isLoading: false,
    }, cb);

    const onError = () => this.setState({
      movies: [],
      isLoading: false,
      hasError: true,
    });

    this.props.movieService.search(query)
      .then(movies => update(movies))
      .catch(() => onError());
  }

  /**
   * Render the app component
   *
   * @return {JSX}
   */
  render() {
    return (
      <div className="app">
        <SearchBar onSearchChange={this.onSearchChange} />
        <SearchBody
          isLoading={this.state.isLoading}
          movies={this.state.movies}
          searchTerm={this.state.searchTerm}
          hasError={this.state.hasError}
        />
      </div>
    );
  }
}

export default App;
