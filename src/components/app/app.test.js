import React from 'react';
import ReactDOM from 'react-dom';

import App from './index';
import SearchBar from '../search-bar';
import SearchBody from '../search-body';

describe('<App />', () => {
  const movieServiceMock = { search: () => {} };

  test('renders without crashing', () => {
    mount(<App movieService={movieServiceMock} />);
  });

  test('a <SearchBar /> is rendered', () => {
    const sut = shallow(<App movieService={movieServiceMock} />);
    const searchbar = sut.find(SearchBar);
    expect(searchbar).toHaveLength(1);
    expect(searchbar).toHaveProp('onSearchChange');
  });

  test('a <SearchBody /> is rendered', () => {
    const sut = shallow(<App movieService={movieServiceMock} />);
    const body = sut.find(SearchBody);
    expect(body).toHaveLength(1);
    expect(body).toHaveProp('movies');
    expect(body).toHaveProp('isLoading');
  });

  test('a search can be performed', () => {
    const mockedService = {
      search: q => Promise.resolve([
        {
          id: 0,
          title: "Foo",
          overview: "Bar",
          poster_path: null,
        }
      ])
    };

    const e = {
      target: {
        value: "hello",
      },
      persist: () => {}
    };

    const sut = shallow(<App movieService={mockedService} />);
    const instance = sut.instance();

    // Debounce breaks when unit testing. Stub it out...
    instance.debounceSearch = query => {
      expect(query).toEqual('hello');
      instance.doSearch(query, () => expect(sut.state().movies).toHaveLength(1));
    };

    instance.onSearchChange(e);
  });

  test('errors are handled', () => {
    const mockedService = {
      search: q => Promise.reject()
    };

    const e = {
      target: {
        value: "hello",
      },
      persist: () => {}
    };

    const sut = shallow(<App movieService={mockedService} />);
    const instance = sut.instance();

    // Debounce breaks when unit testing. Stub it out...
    instance.debounceSearch = query => {
      expect(query).toEqual('hello');
      instance.doSearch(query, () => expect(sut.state().hasError).toBe(true));
    };

    instance.onSearchChange(e);
  });

  test('a search is not performed when the query is empty', () => {
    const e = {
      target: {
        value: "",
      },
    };

    const sut = shallow(<App movieService={movieServiceMock} />);
    const instance = sut.instance();
    const spy = jest.spyOn(instance, 'doSearch');

    instance.onSearchChange(e);

    expect(spy).not.toBeCalled();
  });
});
