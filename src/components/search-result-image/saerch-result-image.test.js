import React from 'react';
import ReactDOM from 'react-dom';

import SearchResultImage from './index';

describe('<SearchResultImage />', () => {
  test('renders without a poster', () => {
    const sut = mount(<SearchResultImage poster={null} />);

    expect(sut).toHaveText('No Image Available');
  });

  test('renders with a poster', () => {
    const sut = mount(<SearchResultImage poster={'/testing.png'} />);

    expect(sut.find('div')).toHaveStyle({
      backgroundImage: 'url(https://image.tmdb.org/t/p/w500/testing.png)',
    });
  });
});
