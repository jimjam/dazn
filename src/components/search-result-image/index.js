import React from 'react';
import PropTypes from 'prop-types';
import './search-result-image.scss';

// Base URL for the image CDN
const PosterBase = 'https://image.tmdb.org/t/p/w500';

/**
 * Search result image component
 *
 * @param {String|null} poster
 * @return {JSX}
 */
const SearchResultImage = ({ poster }) => {
  if (!poster) {
    return <div className="result--noimg">No Image Available</div>;
  }

  const css = { backgroundImage: `url(${PosterBase}${poster})` };

  return <div className="result--img" style={css} />;
};

// Prop types definition
SearchResultImage.propTypes = {
  poster: PropTypes.string,
};

// Default prop types
SearchResultImage.defaultProps = {
  poster: null,
};

export default SearchResultImage;
