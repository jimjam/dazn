import React from 'react';
import ReactDOM from 'react-dom';

import SearchResults from './index';
import SearchResult from '../search-result';

describe('<SearchResults />', () => {
  const data = [
    {
      id: 1,
      title: 'foo',
      overview: 'foo overview',
      poster_path: null,
    },
    {
      id: 2,
      title: 'bar',
      overview: 'bar overview',
      poster_path: '/bar.png',
    },
    {
      id: 3,
      title: 'baz',
      overview: 'baz overview',
      poster_path: '/baz.png',
    },
  ];

  test('renders without crashing', () => {
    mount(<SearchResults movies={data} />);
  });

  test('it renders <SearchResult /> nodes', () => {
    const sut = shallow(<SearchResults movies={data} />);
    const results = sut.find(SearchResult);
    expect(results).toHaveLength(3);
  });
});
