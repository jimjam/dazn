import React from 'react';
import PropTypes from 'prop-types';
import './search-results.scss';
import SearchResult from '../search-result';

/**
 * Describes the search results component
 *
 * @param {Object[]} movies
 * @return {JSX}
 */
const SearchResults = ({ movies }) => (
  <div className="results">
    {movies.map(movie => (
      <SearchResult
        title={movie.title}
        overview={movie.overview}
        poster={movie.poster_path}
        key={movie.id}
      />
    ))}
  </div>
);

// Prop types definition
SearchResults.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default SearchResults;
