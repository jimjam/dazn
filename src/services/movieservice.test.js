import movieService from './movieservice';

describe('Movie Service', () => {
  test('it has a search function', () => {
    const sut = movieService({ key: 'foo', baseUrl: 'bar' });
    expect(sut).toHaveProperty('search');
  });

  test('it can perform a search', async () => {
    // Mock the fetch function
    const mockFetch = jest.fn();
    global.fetch = mockFetch.mockImplementation(() => Promise.resolve({
      ok: true,
      json: () => Promise.resolve({ results: [ {a: 'b'}, {c: 'd'} ] })
    }));


    const actual = await movieService({ key: 'foo', baseUrl: 'http://test.com' }).search('bar');

    expect(mockFetch).toBeCalledWith('http://test.com/search/movie?api_key=foo&query=bar');
    expect(actual).toHaveLength(2);
    expect(actual[0]).toHaveProperty('a');
    expect(actual[0].a).toEqual('b');
    expect(actual[1]).toHaveProperty('c');
    expect(actual[1].c).toEqual('d');
  });
});
