// Encode a key value URL component
const encode = (key, value) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;

// Cast an object of params to a URL params string. {a:1,b:2} => a=1&b=2
const toUrlParams = params => Object.keys(params).map(key => encode(key, params[key])).join('&');

/**
 * Generate a URL
 *
 * @param {String} baseUrl
 * @param {String} apiKey
 * @param {String} path
 * @param {Object} params
 */
const generateUrl = (baseUrl, apiKey, path, params) => {
  const parameters = Object.assign({ api_key: apiKey }, params);

  return `${baseUrl}${path}?${toUrlParams(parameters)}`;
};

/**
 * Run a fetch against the API
 *
 * @param {String} url
 * @return {Promise}
 */
const fetchFromApi = async (url) => {
  const response = await fetch(url);

  return response.json().then(json => json.results);
};

/**
 * Default export - the movie service
 *
 * @param {Object} config The movie service config
 */
export default function ({ baseUrl, key }) {
  const buildUrl = (path, params) => generateUrl(baseUrl, key, path, params);

  return {
    /**
     * Search function
     *
     * @param {String} query
     * @return {Promise}
     */
    search: query => fetchFromApi(buildUrl('/search/movie', { query })),
  };
}
